import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:ventas/helper/sharedPreference.dart';
import 'package:ventas/models/Product.model.dart';
import 'package:ventas/models/User.model.dart';
import 'package:ventas/views/AddProduct.dart';

class RestProvider {
  final _prefs = PreferenceProvider();

  String _baseUrl = 'https://app-ventas-backend.herokuapp.com';

  Map<String, String> _headerJson = {
    'Content-Type': 'application/json',
  };
/*
  Future<List<Quote>> getAllQuotes() async {
    String url = '$_baseUrl/quote';
    http.Response resp = await http.get(url, headers: _headerJson);
    if (resp.statusCode == 200) {
      print('Respuestaa');
      List<dynamic> json = jsonDecode(resp.body);
      return List<Quote>.from(
        json.map(
          (mapJson) => Quote.fromJson(mapJson),
        ),
      );
    } else {
      print('Fue un error');
      return [];
    }
  }

  Future<bool> registerUser(var user) async {
    String url = '$_baseUrl/user/register';

    http.Response resp = await http.post(
      url,
      headers: _headerJson,
      body: jsonEncode(user.toJsonRegister()),
    );
    if (resp.statusCode == 200) {
      print('request register');
      return true;
    } else {
      return false;
    }
  }


  Future<bool> createQuote(Quote quote) async {
    String url = '$_baseUrl/quote/createQuote';
    Map<String, String> _header = {
      'Content-Type': 'application/json',
      'token': await _prefs.getDataString(KeyList.TOKEN),
    };
    http.Response resp = await http.post(
      url,
      headers: _header,
      body: jsonEncode(quote.toJson()),
    );
    if (resp.statusCode == 200) {
      print('create quote');
      print(resp.body);
      return true;
    } else {
      print('create quote fail');
      print(resp.body);
      return false;
    }
  }

  Future<bool> deleteQuote(Quote quote) async {
    String url = '$_baseUrl/quote/deleteQuote/${quote.id}';
    Map<String, String> _header = {
      'Content-Type': 'application/json',
      'token': await _prefs.getDataString(KeyList.TOKEN),
    };
    http.Response resp = await http.delete(url, headers: _header);
    if (resp.statusCode == 200) {
      print('delete quote');
      print(resp.body);
      return true;
    } else {
      print('delete quote fail');
      print(resp.body);
      return false;
    }
  }

  Future<bool> updateQuote(Quote quote) async {
    quote.quote = 'Frase modificada desde el metodo';

    String url = '$_baseUrl/quote/updateQuote/${quote.id}';

    Map<String, String> _header = {
      'Content-Type': 'application/json',
      'token': await _prefs.getDataString(KeyList.TOKEN),
    };

    http.Response resp = await http.put(
      url,
      headers: _header,
      body: jsonEncode(quote.toJson()),
    );

    if (resp.statusCode == 200) {
      print('update quote');
      print(resp.body);
      return true;
    } else {
      print('update quote fail');
      print(resp.body);
      return false;
    }
  }
*/

  Future<bool> loginUser1(UserModel user) async {
    String url = '$_baseUrl/signin';

    //print(user.toJsonLogin());
    try {
      http.Response resp = await http.post(
        url,
        headers: _headerJson,
        body: jsonEncode(user.toJsonLogin()),
      );

      var x = jsonDecode(resp.body);
      if (x['message'] == "internal server error") {
        print(x['message']);
        return false;
      }
      UserModel usertmp = UserModel.fromJson(x['data'][0]);
      //print(resp.statusCode);

      //print(resp.headers['auth-token']);
      //print(x['data']);
      //print(usertmp.correo);
      //USER_ID, USER_NAME, USER_PASSWORD, TOKEN
      _prefs.saveDataString(KeyList.USER_ID, (usertmp.id_usuario).toString());
      _prefs.saveDataString(KeyList.USER_NAME, usertmp.nombre);
      _prefs.saveDataString(KeyList.USER_PASSWORD, usertmp.pasword);
      _prefs.saveDataString(KeyList.ROL, usertmp.rol_usuario);
      _prefs.saveDataString(KeyList.TOKEN, resp.headers['auth-token']);
      //print(await _prefs.getDataString(KeyList.ROL));
      return true;
    } catch (err) {
      return false;
    }
    return false;
  }

  Future<String> registerUser(UserModel user) async {
    //print(user.rol_usuario);
    String url = '$_baseUrl/signup';

    Map<String, String> _headerJson = {
      'Content-Type': 'application/json',
    };

    http.Response resp = await http
        .post(
      url,
      headers: _headerJson,
      body: jsonEncode(user.toJsonRegister()),
    )
        .catchError((error) {
      print(error);
    });

    //print(resp.statusCode);
    //print(resp.body);
    if (resp.statusCode == 200) {
      //print('request usuarios');
      var x = jsonDecode(resp.body);
      //print(x['message']);
      UserModel usertmp = UserModel.fromJson(x['body']);
      //print(usertmp.correo);
      return x['message'];
    } else {
      //print('error peticion');
      //print(resp.body);
      return resp.body;
    }
  }

  Future<List<ProductModel>> getProduct() async {
    //print(await _prefs.getDataString(KeyList.ROL));
    String TOKEN = await _prefs.getDataString(KeyList.TOKEN);
    String url = '$_baseUrl/producto';
    Map<String, String> headerJson = {
      'Content-Type': 'application/json',
      'auth-token': TOKEN
    };
    http.Response resp = await http
        .get(
      url,
      headers: headerJson,
    )
        .catchError((error) {
      print(error);
    });
    if (resp.statusCode == 200) {
      //print("*****************************************");

      List<dynamic> json = jsonDecode(resp.body);
      //print(json);
      //return null;
      return List<ProductModel>.from(
        json.map(
          (mapJson) => ProductModel.fromJson(mapJson),
        ),
      );

      //print("*****************************************");
      //return null;
    } else {
      //print("error status");
      return null;
    }
  }

  Future<List<ProductModel>> getProductCategory(String categoria) async {
    //print(await _prefs.getDataString(KeyList.ROL));
    String TOKEN = await _prefs.getDataString(KeyList.TOKEN);
    String url = '$_baseUrl/producto/categoria';
    Map<String, String> headerJson = {
      'Content-Type': 'application/json',
      'auth-token': TOKEN,
      'categoria': categoria
    };
    http.Response resp = await http
        .get(
      url,
      headers: headerJson,
    )
        .catchError((error) {
      print(error);
    });
    if (resp.statusCode == 200) {
      //print("*****************************************");

      List<dynamic> json = jsonDecode(resp.body);
      //print(json);
      //return null;
      return List<ProductModel>.from(
        json.map(
          (mapJson) => ProductModel.fromJson(mapJson),
        ),
      );

      //print("*****************************************");
      return null;
    } else {
      print("error status");
      return null;
    }
  }

  Future profile() async {
    String TOKEN = await _prefs.getDataString(KeyList.TOKEN);
    String url = '$_baseUrl/profile';
    Map<String, String> headerJson = {
      'Content-Type': 'application/json',
      'auth-token': TOKEN
    };
    http.Response resp = await http
        .get(
          url,
          headers: headerJson,
        )
        .catchError((err) => {print(err)});
    if (resp.statusCode == 200) {
      //print(jsonDecode(resp.body)[0]);
      UserModel user = UserModel.fromJson(jsonDecode(resp.body)[0]);
      //print(user.correo);
      //print(user.id_usuario);
      return user;
    }
    return null;
  }

  Future<List<UserModel>> getUser() async {
    //print(await _prefs.getDataString(KeyList.ROL));
    String TOKEN = await _prefs.getDataString(KeyList.TOKEN);
    String url = '$_baseUrl/usuarios';
    Map<String, String> headerJson = {
      'Content-Type': 'application/json',
      'auth-token': TOKEN
    };
    http.Response resp = await http
        .get(
      url,
      headers: headerJson,
    )
        .catchError((error) {
      print(error);
    });
    if (resp.statusCode == 200) {
      //print("*****************************************");

      List<dynamic> json = jsonDecode(resp.body);
      //print(json);
      //return null;
      return List<UserModel>.from(
        json.map(
          (mapJson) => UserModel.fromJson(mapJson),
        ),
      );

      //print("*****************************************");
      return null;
    } else {
      //print("error status");
      return null;
    }
  }

  Future AddProduct(ProductModel productModel) async {
    String url = '$_baseUrl/producto/add';
    String TOKEN = await _prefs.getDataString(KeyList.TOKEN);
    Map<String, String> _headerJson = {
      'Content-Type': 'application/json',
      'auth-token': TOKEN
    };
    http.Response resp = await http
        .post(
      url,
      headers: _headerJson,
      body: jsonEncode(productModel.toJsonRegister()),
    )
        .catchError((error) {
      print(error);
    });
    //print(resp.statusCode);
    //print(resp.body);
    if (resp.statusCode == 200) {
      //print('request usuarios');
      //print(jsonDecode(resp.body)[0]);
      //print(x['message']);
      ProductModel producttmp = ProductModel.fromJson(jsonDecode(resp.body)[0]);
      //print(producttmp.nombre);
      //print(usertmp.correo);
      return producttmp;
    } else {
      //print('error peticion');
      //print(resp.body);
      return null;
    }
  }

  // ! agregar editar profile
  Future editProfile() async {
    String TOKEN = await _prefs.getDataString(KeyList.TOKEN);
    String url = '$_baseUrl/usuarios';
    Map<String, String> headerJson = {
      'Content-Type': 'application/json',
      'auth-token': TOKEN
    };
  }

  Future<String> delUser(int id) async {
    String TOKEN = await _prefs.getDataString(KeyList.TOKEN);
    String url = '$_baseUrl/profile/delete/$id';
    Map<String, String> headerJson = {
      'Content-Type': 'application/json',
      'auth-token': TOKEN
    };
    http.Response resp = await http.delete(url,headers: headerJson);

    if(resp.statusCode == 200){
      return resp.body;
    }
    return "Ocurrio Un error";
  }
  Future<String> delProduct(int id) async {
    String TOKEN = await _prefs.getDataString(KeyList.TOKEN);
    String url = '$_baseUrl/producto/delete/$id';
    Map<String, String> headerJson = {
      'Content-Type': 'application/json',
      'auth-token': TOKEN
    };
    http.Response resp = await http.delete(url,headers: headerJson);

    if(resp.statusCode == 200){
      return resp.body;
    }
    return "Ocurrio Un error";
  }
}
