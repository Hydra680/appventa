import 'package:flutter/material.dart';
import 'package:ventas/views/AdminPanel.dart';
import 'package:ventas/views/HomePage.dart';
import 'package:ventas/views/SigIn.dart';
import 'package:ventas/views/SigUp.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/sigUp',
      routes: {
        '/sigUp': (BuildContext context) => SigUp(),
        '/sigIn': (BuildContext context) => SigIn(),
        '/home': (BuildContext context) => HomePage(),
        '/AdminPanel': (BuildContext context) => AdminPanel(),
      },
    );
  }
}
