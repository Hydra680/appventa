import 'package:flutter/material.dart';

class AddPromocion extends StatefulWidget {
  @override
  _AddPromocionState createState() => _AddPromocionState();
}

class _AddPromocionState extends State<AddPromocion> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 26,
        ),
        height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Registrar Promocion",
              style: TextStyle(fontSize: 24),
            ),
            SizedBox(
              height: 36,
            ),
            TextField(
              decoration: InputDecoration(
                labelText: "Nombre Promocion",
                prefixIcon: Icon(Icons.add_box),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            TextField(
              decoration: InputDecoration(
                labelText: "Descripcion",
                prefixIcon: Icon(Icons.add_box),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            TextField(
              decoration: InputDecoration(
                labelText: "Fecha Inicio",
                prefixIcon: Icon(Icons.add_box),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            TextField(
              decoration: InputDecoration(
                labelText: "Fecha Finalizacion",
                prefixIcon: Icon(Icons.add_box),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text("Añadir Imagen"),
                IconButton(
                  icon: Icon(Icons.camera),
                  onPressed: null,
                ),
                IconButton(
                  icon: Icon(Icons.picture_in_picture),
                  onPressed: null,
                ),
                SizedBox(
                  height: 16,
                ),
              ],
            ),
            RaisedButton(
              padding: EdgeInsets.symmetric(horizontal: 25),
              onPressed: () {},
              shape: StadiumBorder(),
              color: Colors.red,
              child: Text("Guardar"),
            ),
          ],
        ),
      ),
    );
  }
}
