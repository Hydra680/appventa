import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:ventas/helper/RestVenta.dart';
import 'package:ventas/models/User.model.dart';

class UsuariosAdmmin extends StatefulWidget {
  @override
  _UsuariosAdmminState createState() => _UsuariosAdmminState();
}

class _UsuariosAdmminState extends State<UsuariosAdmmin> {
  RestProvider rest = RestProvider();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
      ),
      body: _listProfile(),
    );
  }

  Widget _listProfile() {
    return FutureBuilder(
        future: rest.getUser(),
        builder: (conext, snapshot) {
          if (snapshot.hasData) {
            List<UserModel> listdata = snapshot.data;
            return ListView.builder(
                itemCount: listdata.length,
                itemBuilder: (context, i) {
                  if (listdata[i].rol_usuario != 'Admin') {
                    return _cardProduct(
                      listdata[i].id_usuario,
                      f_n: "12/12/2020",
                      correo: "${listdata[i].correo}",
                      telefono: "680989898",
                      nombre: "${listdata[i].nombre}",
                      networkimg: "${listdata[i].url_image}",
                    );
                  }
                  return Container();
                });
          } else {
            return Center(child: CircularProgressIndicator());
          }
        });
  }

  Widget _cardProduct(int id,
      {String nombre = "Nombres y apellidos",
      @required String networkimg,
      @required String correo,
      @required String telefono,
      @required String f_n}) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16),
      child: InkWell(
        onTap: () async{
          print(id);
        },
        child: Card(
          child: Column(
            children: [
              Text(
                "${nombre}",
                style: TextStyle(fontSize: 24),
              ),
              Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.symmetric(
                        vertical: 16,
                        horizontal: 12,
                      ),
                      height: 250,
                      width: 250,
                      child: CircleAvatar(
                        backgroundImage: NetworkImage(
                          "${networkimg}",
                        ),
                        backgroundColor: Colors.transparent,
                        radius: 120,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.only(right: 8),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Datos"),
                          SizedBox(
                            height: 16,
                          ),
                          Text(
                            "Correo: ${correo}",
                            style: TextStyle(fontSize: 10),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Text(
                            "Telefono: ${telefono}",
                            style: TextStyle(fontSize: 10),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Text(
                            "Fecha Nacimiento: ${f_n}",
                            style: TextStyle(fontSize: 10),
                          ),
                          SizedBox(
                            height: 12,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  RaisedButton(
                    color: Colors.red,
                    shape: StadiumBorder(),
                    onPressed: () async{
                      await rest.delUser(id).then((value) => Toast.show("$value", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM));
                      setState(() {
                        print("${id}");
                        print("${networkimg}");

                        
                      });
                    },
                    child: Container(
                      width: 100,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Icon(
                            Icons.delete,
                            color: Colors.white,
                          ),
                          Text(
                            "Delete",
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                  RaisedButton(
                    color: Colors.red,
                    shape: StadiumBorder(),
                    onPressed: () {},
                    child: Container(
                      width: 100,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Icon(
                            Icons.edit,
                            color: Colors.white,
                          ),
                          Text(
                            "Edit",
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
