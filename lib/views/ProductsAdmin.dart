import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:ventas/helper/RestVenta.dart';
import 'package:ventas/models/Product.model.dart';

class ProductAdmin extends StatefulWidget {
  @override
  _ProductAdminState createState() => _ProductAdminState();
}

class _ProductAdminState extends State<ProductAdmin> {
  RestProvider rest = RestProvider();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text("Administrar Productos"),
      ),
      body: _body(),
    );
  }
  Widget _body(){
    return Container(
      child: FutureBuilder(
        future: rest.getProduct(),
        builder: (context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            List<ProductModel> data = snapshot.data;
            return ListView.builder(
              itemCount: data.length,
              itemBuilder: (context, i) {
                return _cardProduct(data[i].id_producto,
                  title: "${data[i].nombre}",
                  descr:
                      "${data[i].descripcion}",
                  networkimg:
                      "${data[i].url_image_product}",
                  precio: "${data[i].cantidad_productos}"
                );
              },
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }

  Widget _cardProduct(int id,
      {String title = "Product",
      String networkimg,
      String descr,
      String precio = "0.0"}) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16),
      child: InkWell(
        onTap: () {
          //print(id);
        },
        child: Card(
          child: Column(
            children: [
              Text(
                "${title}",
                style: TextStyle(fontSize: 24),
              ),
              Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.symmetric(
                        vertical: 16,
                        horizontal: 12,
                      ),
                      height: 250,
                      width: 250,
                      child: Image.network(
                        "${networkimg}",
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.only(right: 8),
                      child: Column(
                        children: [
                          Text("Descipcion"),
                          Text(
                            "${descr}",
                            style: TextStyle(fontSize: 10),
                          ),
                          SizedBox(
                            height: 12,
                          ),
                          Row(
                            children: [
                              Icon(Icons.attach_money),
                              Text("${precio} Bs")
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  RaisedButton(
                    color: Colors.red,
                    shape: StadiumBorder(),
                    onPressed: () async{
                      // ! quwery delete
                      try{
                         var data = await rest.delProduct(id);
                         Toast.show("${data != null ? 'Se Elimino con Exito':'Error' }", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
                         setState(() {
                           
                         });

                      }catch(e){
                          Toast.show("Ocurrio un Error", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);
                      }
                      print(id);
                    },
                    child: Container(
                      width: 100,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Icon(
                            Icons.delete,
                            color: Colors.white,
                          ),
                          Text(
                            "Delete",
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                  RaisedButton(
                    color: Colors.red,
                    shape: StadiumBorder(),
                    onPressed: () {},
                    child: Container(
                      width: 100,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Icon(Icons.edit, color: Colors.white),
                          Text(
                            "Edit",
                            style: TextStyle(color: Colors.white),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
