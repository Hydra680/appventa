import 'package:flutter/material.dart';
import 'package:ventas/helper/RestVenta.dart';
import 'package:ventas/models/User.model.dart';
import 'package:ventas/views/AddProduct.dart';
import 'package:ventas/views/AddPromocion.dart';
import 'package:ventas/views/DeletePromo.dart';
import 'package:ventas/views/ProductsAdmin.dart';
import 'package:ventas/views/UsersAdmin.dart';

class AdminPanel extends StatefulWidget {
  @override
  _AdminPanelState createState() => _AdminPanelState();
}

class _AdminPanelState extends State<AdminPanel> {
  bool _edit = false;
  RestProvider rest = RestProvider();

  String url = null;
  String name = null;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DrawerApp(),
      appBar: _appBar(),
      body: _bodyApp(),
    );
  }

  Widget _appBar() {
    return AppBar(
      backgroundColor: Colors.red,
      title: Text(name == null ? "Usuario" : name),
    );
  }

  Widget _bodyApp() {
    return Stack(
      alignment: Alignment.center,
      children: [
        _body1(),
        SingleChildScrollView(
          padding: EdgeInsets.only(top: 32),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              CircleAvatar(
                backgroundImage: NetworkImage(
                  url == null
                      ? "https://fotografias.lasexta.com/clipping/cmsimages02/2019/11/14/66C024AF-E20B-49A5-8BC3-A21DD22B96E6/69.jpg"
                      : url,
                ),
                backgroundColor: Colors.black,
                radius: 120,
              ),

              SizedBox(
                height: 56,
              ),
              FutureBuilder(
                future: rest.profile(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    UserModel user = snapshot.data;

                    url = user.url_image;
                    name = user.nombre;

                    return _body(nombre: user.nombre, correo: user.correo);
                  }
                  return Center(
                    child: CircularProgressIndicator(
                      backgroundColor: Colors.black,
                    ),
                  );
                },
              ),
              //_body()
            ],
          ),
        ),
      ],
    );
  }

  Widget _body1() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: [
          Expanded(
            flex: 1,
            child: Container(
              color: Colors.red,
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(),
          ),
        ],
      ),
    );
  }

  Widget _body({String correo, String nombre}) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            "${nombre}",
            style: TextStyle(fontSize: 36),
          ),
          SizedBox(
            height: 25,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 36),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _edit ? _editProfile() : _profile(correo: correo),
                SizedBox(
                  height: 64,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _editProfile() {
    return Column(
      children: [
        TextField(
          keyboardType: TextInputType.emailAddress,
          decoration: InputDecoration.collapsed(
            hintText: "correo",
          ),
          maxLines: 1,
        ),
        SizedBox(
          height: 16,
        ),
        TextField(
          decoration: InputDecoration.collapsed(
            hintText: "Nueva contraseña",
          ),
          maxLines: 1,
          keyboardType: TextInputType.visiblePassword,
        ),
        SizedBox(
          height: 16,
        ),
        TextField(
          decoration: InputDecoration.collapsed(
            hintText: "Ingrese de nuevo la contraseña",
          ),
          maxLines: 1,
          keyboardType: TextInputType.visiblePassword,
        ),
        SizedBox(
          height: 16,
        ),
        Divider(
          color: Colors.red,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RaisedButton(
              padding: EdgeInsets.symmetric(horizontal: 64),
              shape: StadiumBorder(),
              color: Colors.red,
              onPressed: () {
                setState(() {
                  _edit = !_edit;
                });
              },
              child: Row(
                children: [
                  Icon(
                    Icons.save,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Text(
                    "Save",
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _profile({String correo}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Correo: ${correo}",
          style: TextStyle(fontSize: 16, color: Colors.grey),
        ),
        SizedBox(
          height: 16,
        ),
        Text(
          "Password: *************",
          style: TextStyle(fontSize: 16, color: Colors.grey),
        ),
        SizedBox(
          height: 16,
        ),
        Divider(
          color: Colors.red,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RaisedButton(
              padding: EdgeInsets.symmetric(horizontal: 64),
              shape: StadiumBorder(),
              color: Colors.red,
              onPressed: () {
                setState(() {
                  _edit = !_edit;
                });
              },
              child: Row(
                children: [
                  Icon(
                    Icons.edit,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Text(
                    "Edit",
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class DrawerApp extends StatefulWidget {
  @override
  _DrawerAppState createState() => _DrawerAppState();
}

class _DrawerAppState extends State<DrawerApp> {
  RestProvider rest = RestProvider();
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: rest.profile(),
      builder: (contex, snapshot) {
      if (snapshot.hasData) {
        UserModel user = snapshot.data;

        return _drawer(nombre: user.nombre,correo: user.correo,image: user.url_image);
      } else {
        return Center(
          child: CircularProgressIndicator(
            backgroundColor: Colors.black,
          ),
        );
      }
    });
  }

  Widget _drawer({String nombre="hydra",String correo= "correo",String image}) {
    return Drawer(
      elevation: 10,
      child: Container(
        child: Column(
          children: [
            UserAccountsDrawerHeader(
              decoration: BoxDecoration(
                color: Colors.red,
              ),
             
              currentAccountPicture: CircleAvatar(
                backgroundImage: NetworkImage(image
                ),
                child: Text(
                  "${nombre[0]}",
                  style: TextStyle(fontSize: 40, color: Colors.redAccent),
                ),
                backgroundColor: Colors.green[800],
              ),
              otherAccountsPictures: [
                CircleAvatar(
                  backgroundColor: Colors.black,
                ),
              ],
              accountName: Text("${nombre}"),
              accountEmail: Text("${correo}"),
            ),
            _listacuerpo(
              Icons.storage,
              "Mis Productos (Editar/Eliminar)",
              func: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ProductAdmin()),
                );
              },
            ),
            _listacuerpo(
              Icons.people,
              "Mis Usuarios",
              func: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => UsuariosAdmmin()),
                );
              },
            ),
            _listacuerpo(Icons.turned_in_not, "Reservas en Cola"),

            //dibuja una linea horizontal
            Divider(
              color: Colors.black54,
            ),
            _listacuerpo(
              Icons.ad_units,
              "Adicionar Producto",
              func: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AddProduct()),
                );
              },
            ),
            _listacuerpo(
              Icons.preview,
              "Adicionar Promocion",
              func: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AddPromocion()),
                );
              },
            ),
            _listacuerpo(
              Icons.delete_forever,
              "Eliminar Promocion",
              func: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => DeletePromo()),
                );
              },
            ),
            //_listacuerpo(Icons.security, "Play Protectet"),
            _listacuerpo(Icons.settings, "Configuración"),
          ],
        ),
      ),
    );
  }

  Widget _listacuerpo(IconData icon, String nombrec, {Function func = null}) {
    return InkWell(
        splashColor: Colors.redAccent,
        onTap: func,
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.only(top: 12, left: 14, bottom: 10),
          child: Row(
            children: <Widget>[
              Icon(
                icon,
                size: 25,
                color: Colors.black54,
              ),
              SizedBox(
                width: 25,
              ),
              Expanded(
                  child: Text(
                nombrec,
                style: TextStyle(
                  fontSize: 15,
                  color: Colors.black87,
                ),
              ))
            ],
          ),
        ));
  }
}
