import 'package:flutter/material.dart';
import 'package:ventas/helper/RestVenta.dart';
import 'package:ventas/models/Product.model.dart';
class ProductsPromo extends StatefulWidget {
  @override
  _ProductsPromoState createState() => _ProductsPromoState();
}

class _ProductsPromoState extends State<ProductsPromo> {
  RestProvider rest = RestProvider();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder(
        future: rest.getProduct(),
           
        builder: (context, AsyncSnapshot snapshot) {
        
          if (snapshot.hasData) {
            List<ProductModel> data = snapshot.data;
            return ListView.builder(
              itemCount: data.length,
              itemBuilder: (context, i) {
                return _cardProduct(
                    title: "${data[i].nombre}",
                    descr: "${data[i].descripcion}",
                    networkimg: "${data[i].url_image_product}",
                    precio: "${data[i].cantidad_productos}");
              },
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }

  Widget _cardProduct(
      {String title = "Product",
      String networkimg,
      String descr,
      String precio = "0.0"}) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16),
      child: InkWell(
        onTap: () {},
        child: Card(
          child: Column(
            children: [
              Text(
                "${title}",
                style: TextStyle(fontSize: 24),
              ),
              Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.symmetric(
                        vertical: 16,
                        horizontal: 12,
                      ),
                      height: 250,
                      width: 250,
                      child: Image.network(
                        "${networkimg}",
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      padding: EdgeInsets.only(right: 8),
                      child: Column(
                        children: [
                          Text("Descipcion"),
                          Text(
                            "${descr}",
                            style: TextStyle(fontSize: 10),
                          ),
                          SizedBox(
                            height: 12,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Icon(Icons.attach_money),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [

                              Text("Antes: ${precio} Bs"),
                              Text("Ahora: ${int.parse(precio) * 0.5} Bs")
                              ],)
                              
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  RaisedButton(
                    color: Colors.red,
                    shape: StadiumBorder(),
                    onPressed: () {
                      showDialog(context: context,
                      child:AlertDialog(content: Text("¿ Desea Agregar el Producto al Carrito ?"),
                      actions: [
                        FlatButton(onPressed: (){}, child: Text("Aceptar")),
                        FlatButton(onPressed: (){}, child: Text("Cancelar")),
                      ],));
                    },
                    child: Container(
                      width: 100,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Icon(
                            Icons.add,
                            color: Colors.white,
                          ),
                          Text(
                            "Agregar",
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}