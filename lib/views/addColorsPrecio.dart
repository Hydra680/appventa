import 'package:flutter/material.dart';

class AddColorsPrecio extends StatefulWidget {
  @override
  _AddColorsPrecioState createState() => _AddColorsPrecioState();
}

class _AddColorsPrecioState extends State<AddColorsPrecio> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 26),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Registrar Colores Y precio",
            style: TextStyle(fontSize: 24),
          ),
          SizedBox(
            height: 32,
          ),
          TextField(
            decoration: InputDecoration(
              labelText: "Color",
              prefixIcon: Icon(Icons.add_box),
            ),
          ),
          SizedBox(
            height: 32,
          ),
          TextField(
            decoration: InputDecoration(
              labelText: "Precio",
              prefixIcon: Icon(Icons.add_box),
            ),
          ),
          SizedBox(
            height: 32,
          ),
          RaisedButton(
            padding: EdgeInsets.symmetric(horizontal: 25),
            onPressed: () {},
            shape: StadiumBorder(),
            color: Colors.red,
            child: Text("Guardar"),
          ),
        ],
      ),
    );
  }
}
