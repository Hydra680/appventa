import 'package:flutter/material.dart';

class AddTallaProduct extends StatefulWidget {
  @override
  _AddTallaProductState createState() => _AddTallaProductState();
}

class _AddTallaProductState extends State<AddTallaProduct> {
  DateTime selectedDate = DateTime.now();
  bool _sw = true;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 26),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Registrar Producto",
            style: TextStyle(fontSize: 24),
          ),
          SizedBox(
            height: 32,
          ),
          TextField(
            decoration: InputDecoration(
              labelText: "Talla",
              prefixIcon: Icon(Icons.add_box),
            ),
          ),
          SizedBox(
            height: 32,
          ),
          RaisedButton(
            onPressed: () {
              _selectDate(context);
              setState(() {
                _sw = !_sw;
              });
            }, // Refer step 3
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Icon(Icons.calendar_today, color: Colors.white),
                Text(
                  'Select date',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            shape: StadiumBorder(),
            color: Colors.black,
          ),
          Text(
            _sw ? "Selecciona La Fecha" : "${selectedDate}",
          ),
          SizedBox(
            height: 32,
          ),
          RaisedButton(
            padding: EdgeInsets.symmetric(horizontal: 25),
            onPressed: () {},
            shape: StadiumBorder(),
            color: Colors.red,
            child: Text("Guardar"),
          ),
        ],
      ),
    );
  }

  _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: selectedDate, // Refer step 1
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        print(selectedDate);
      });
  }
}
