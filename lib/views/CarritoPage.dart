import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:ventas/helper/sharedPreference.dart';

class CarritoPage extends StatefulWidget {
  @override
  _CarritoPageState createState() => _CarritoPageState();
}

class _CarritoPageState extends State<CarritoPage> {
  bool sw = false;
  final _prefs = PreferenceProvider();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: _body(),
    );
  }

  Widget _body() {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 25,
        vertical: 15,
      ),
      child: SingleChildScrollView(
        child: Column(
          children: [
             
            _listCarr(),
          ],
        ),
      ),
    );
  }

  Widget _listCarr() {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            "Valide Sus Reservas",
            style: TextStyle(fontSize: 24),
          ),
          sw
              ? 
              Column(children: [
                Text("Su codigo Qr Para el Reclamo de su reservas es:"),
                  QrImage(
                    data: "1234567890",
                    version: QrVersions.auto,
                    size: 250.0,
                  )
              ],)
              : Container(),
          Card(
            child: Container(
              color: Colors.redAccent[50],
              margin: EdgeInsets.only(top: 16, right: 16, left: 16),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text("Tus Reservas Son: "),
                      Text("Fecha:2020/25/14"),
                    ],
                  ),
                  Divider(),
                  _lost2(),
                  Divider(),
                  RaisedButton(
                    shape: StadiumBorder(),
                    color: Colors.red,
                    onPressed: () {
                      if (!sw) {
                        setState(() {
                          sw = true;
                          print(sw);
                        });
                      }
                    },
                    child: Text("Valide Su Reserva"),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _lost2() {
    return Container(
      height: 500,
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            
            ListTile(
              leading: Image(image: NetworkImage("https://kiaraimportaciones.com/wp-content/uploads/2020/01/Calzados-de-tacon-boca-de-pescado-IKAROS-negro-Z042.jpg")
              ,height: 250, width: 81,
              fit:BoxFit.cover,),
              title: Text('Calzados'),
              subtitle: Text(
                  'P/U: 250 BS\n\nLorem Ipsum es simplemente el texto de relleno . '),
              trailing: _leftList(),
              onTap: () {
                print("s");
              },
            ),           
            ListTile(
              leading: Image(image: NetworkImage("https://kiaraimportaciones.com/wp-content/uploads/2020/01/Calzados-de-tacon-boca-de-pescado-IKAROS-negro-Z042.jpg")
              ,height: 250, width: 81,
              fit:BoxFit.cover,),
              title: Text('Calzados'),
              subtitle: Text(
                  'P/U: 250 BS\n\nLorem Ipsum es simplemente el texto de relleno . '),
              trailing: _leftList(),
              onTap: () {
                print("s");
              },
            ),
            ListTile(
              leading: Image(image: NetworkImage("https://kiaraimportaciones.com/wp-content/uploads/2020/01/Calzados-de-tacon-boca-de-pescado-IKAROS-negro-Z042.jpg")
              ,height: 250, width: 81,
              fit:BoxFit.cover,),
              title: Text('Calzados'),
              subtitle: Text(
                  'P/U: 250 BS\n\nLorem Ipsum es simplemente el texto de relleno . '),
              trailing: _leftList(),
              onTap: () {
                print("s");
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _leftList() {
    return Container(
      width: 100,
      height: 100,
      child: Column(
        children: [
          Row(
            children: [
              IconButton(
                icon: Icon(
                  Icons.delete,
                  size: 24,
                ),
                onPressed: () {},
                iconSize: 24,
                color: Colors.red,
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _appBar() {
    return AppBar(
      backgroundColor: Colors.red,
      title: FutureBuilder<String>(
        future: _prefs.getDataString(KeyList.USER_NAME),
        // ignore: missing_return
        builder: (context, AsyncSnapshot<String> snapshot) {
          if (snapshot.hasData) {
            //print("data" + snapshot.data);
            return Text(snapshot.data);
          } else {
            return Text("Not data");
          }
        },
      ),
    );
  }
}
