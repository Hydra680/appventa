import 'package:flutter/material.dart';
import 'package:ventas/helper/RestVenta.dart';
import 'package:ventas/helper/sharedPreference.dart';
import 'package:ventas/models/User.model.dart';
import 'package:ventas/widget/paintSemiCircle.dart';
import 'dart:math' as math;
import 'package:toast/toast.dart';

class SigUp extends StatefulWidget {
  @override
  _SigUpState createState() => _SigUpState();
}

class _SigUpState extends State<SigUp> {
  //variables
  bool correo = true;
  bool obscurep = true;
  final _Rest = RestProvider();
  TextEditingController correoC = TextEditingController();
  TextEditingController passwordC = TextEditingController();
  final _prefs = PreferenceProvider();
  bool isload = true;

  @override
  Widget build(BuildContext context) {
    //print(MediaQuery.of(context).size.height);
    return Scaffold(
      body: isload ? _body(): Center(child: CircularProgressIndicator(backgroundColor: Colors.redAccent)),
    );
  }

  Widget _fondo1() {
    return Column(
      children: [
        CustomPaint(
          painter: MyPainter(startAngle: math.pi * 4 / 2, data: Colors.red),
          size: Size(MediaQuery.of(context).size.width,
              MediaQuery.of(context).size.height - 440),
        ),
        Container(
          height: 35,
        ),
        CustomPaint(
          painter: MyPainter(startAngle: math.pi, data: Colors.red),
          size: Size(MediaQuery.of(context).size.width,
              MediaQuery.of(context).size.height - 440),
        ),
      ],
    );
  }

  Widget _body() {
    return SingleChildScrollView(
      child: Stack(
        alignment: Alignment.center,
        children: [
          _fondo1(),
          _logIn(),
        ],
      ),
    );
  }

  Widget _logIn() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 25),
            decoration: BoxDecoration(
              color: Colors.black45,
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              ),
            ),
            child: Column(
              children: [
                SizedBox(
                  height: 15,
                ),
                Text(
                  "Log In",
                  style: TextStyle(color: Colors.black, fontSize: 25),
                ),
                SizedBox(
                  height: 15,
                ),
                _form(
                  textEditingController: correoC,
                  labelText: "Correo",
                  mesageError: "Coloque su email",
                  icon: Icons.mail_outline,
                  text: TextInputType.emailAddress,
                ),
                SizedBox(
                  height: 15,
                ),
                _form(
                    textEditingController: passwordC,
                    labelText: "Password",
                    mesageError: "Coloque su password",
                    text: TextInputType.visiblePassword,
                    icon: obscurep ? Icons.lock_outline : Icons.lock_open,
                    obscure: obscurep,
                    f: () {
                      setState(() {
                        obscurep = !obscurep;
                      });
                    }),
                _formRegistro(),
                RaisedButton(
                    colorBrightness: Brightness.dark,
                    color: Colors.black,
                    padding: EdgeInsets.symmetric(horizontal: 45),
                    shape: StadiumBorder(),
                    child: Text("Log In"),
                    onPressed: () async {
                       showDialog(context: context,
                        child: Center(child: CircularProgressIndicator(),));
                      //String email = "usuario1@gmail.com";
                      //String email = 'hydra1@gmail.com';
                      //String email = 'hy@hy.com';
                      //String password = "123456789";

                      String email = correoC.text.trim();
                      String password = passwordC.text.trim();
                      UserModel n =
                          new UserModel(correo: email, pasword: password);
                      bool ini =
                          await _Rest.loginUser1(n).catchError((error) => {
                                Toast.show("Ocurrio un error", context,
                                    duration: Toast.LENGTH_SHORT,
                                    gravity: Toast.BOTTOM)
                              });
                      //print(ini);
                      String rol = await _prefs.getDataString(KeyList.ROL);
                      //print(rol);
                      setState(() {
                        isload = ! isload;
                      });

                      if (ini && rol == 'Usuario') {
                       
                        Toast.show("Usuario", context,
                            duration: Toast.LENGTH_SHORT,
                            gravity: Toast.BOTTOM);
                        Navigator.pushNamed(context, '/home');
                        
                        
                      } else if (ini && rol == 'Admin') {
                        Toast.show("Admin", context,
                            duration: Toast.LENGTH_SHORT,
                            gravity: Toast.BOTTOM);
                        Navigator.pushNamed(context, '/AdminPanel');
                       
                      } else {
                        Toast.show("Usuairo o contraseña invalido", context,
                            duration: Toast.LENGTH_SHORT,
                            gravity: Toast.BOTTOM);
                      }
                    }),
                SizedBox(
                  height: 15,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _formRegistro() {
    return Padding(
      padding: const EdgeInsets.only(top: 12, left: 36, bottom: 16),
      child: Row(
        children: [
          Text("No tiene cuenta? desea ",
              style: TextStyle(
                color: Colors.white,
              )),
          GestureDetector(
            onTap: () {
              Navigator.pushNamed(
                context,
                '/sigIn',
              );
            },
            child: Hero(
              transitionOnUserGestures: true,
              tag: 'registro',
              child: Text("REGISTRARSE..",
                  style: TextStyle(
                    color: Colors.white,
                  )),
            ),
          )
        ],
      ),
    );
  }

  Widget _form(
      {String labelText,
      String mesageError,
      TextInputType text,
      bool obscure = false,
      IconData icon,
      Function f = null,
      TextEditingController textEditingController}) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 25),
      child: TextField(
        controller: textEditingController,
        maxLines: 1,
        obscureText: obscure,
        onChanged: (cad) {
          setState(() {
            if (!(cad.length > 0)) {
              correo = false;
            } else
              correo = true;
          });
        },
        keyboardType: text,
        decoration: InputDecoration(
            prefix: IconButton(
              icon: Icon(icon),
              onPressed: f,
            ),
            hoverColor: Colors.black,
            labelText: labelText,
            labelStyle: TextStyle(color: Colors.black87),
            errorText: correo ? null : mesageError,
            border: UnderlineInputBorder()),
      ),
    );
  }
}
