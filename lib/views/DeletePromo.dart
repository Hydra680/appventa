import 'package:flutter/material.dart';

class DeletePromo extends StatefulWidget {
  @override
  _DeletePromoState createState() => _DeletePromoState();
}

class _DeletePromoState extends State<DeletePromo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: Text("Promociones"),
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return ListView(
      children: [
        _promociones(nameP: "Verano"),
        _promociones(nameP: "Verano"),
        _promociones(nameP: "Verano"),
      ],
    );
  }

  Widget _promociones({String nameP}) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 15,
      ),
      child: Card(
        child: Column(
          children: [
            Text(
              "${nameP}",
              style: TextStyle(fontSize: 26),
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 16,
              ),
              child: Image.network(
                "https://media.istockphoto.com/vectors/banner-summer-sale-discounts-advertising-of-shop-and-boutique-of-vector-id994819900",
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(
              height: 16,
            ),
            Text("Descripcion"),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 32, vertical: 16),
              child: Text(
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 32, vertical: 8),
              child: RaisedButton(
                  color: Colors.red,
                  shape: StadiumBorder(),
                  onPressed: () {},
                  child: Row(
                    children: [
                      Icon(Icons.delete, color: Colors.white),
                      Text(
                        "Eliminar Promo",
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      )
                    ],
                  )),
            )
          ],
        ),
      ),
    );
  }
}
